# FootballField

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Tentang Aplikasi ini:
Aplikasi ini menggunakan framework Angular 12 , dan sebelum melakukan serve, harap melakukan npm i terlebih dahulu.
Aplikasi ini dibuat untuk membantu user untuk melihat seluruh area yang ada, dan melihat klub sepakbola yang ada di area yang dipilih.
Selain itu juga aplikasi ini dapat melihat profile dari klub yang dipilih, list pemain dan detail dari masing-masing pemain.

Pada tahap development, saya melakukan define variable scss di bagian style.scss dan variable.scss. Alasannya adalah agar membantu dalam proses pengembangan. Dengan melakukan define variable di main,variable, dan style scss, saya jadi lebih cepat untuk melakukan development aplikasi. 

Untuk design saya menggunakan bootstrap dan juga ngx bootstrap. Alasan pemilihan ke 2 tools ini karena ke 2 tools ini sangat mudah untuk dipahami dan mudah untuk diimplementasikan pada aplikasi.

Pada awal development, saya mengalami masalah pada bagian cors, tapi bagian itu bisa dilewati dengan menggunakan extension pada firefox yang bernama "CORS Everywhere". 

Di aplikasi ini juga saya membuat layout sendiri dengan alasan untuk mempercantik tampilan yang ada. sehingga aplikasi hanya  akan berada di 1 tab yang sama, tapi dengan isi yang berbeda. Dengan melakukan ini, aplikasi jadi lebih rapi dan bersih. Pada aplikasi ini juga saya memasukkan HTTP Interceptor karena syarat untuk melakuka hit api di source footbal ada dengan adanya X-Auth-Token pada bagian header. Karena itu saya memasukan HTTP Interceptor untuk menambahkan X-Auth-Token.

Pada saat user membuka aplikasi (melakukan serve), saya langsung melakukan hit API untuk menampilkan seluruh area yang ada. Area itu bisa diklik untuk menampilkan button cek team. Saat di klik, akan muncul team-team yang ada di bawah area yang bersangkutan. Team-team itu juga dapat di klik untuk menampilkan profile dari tim termasuk list dari pemain. Sementara list pemain dapat diklik untuk membuka modal yang menampilkan profil dari pemain yang dimaksud.
