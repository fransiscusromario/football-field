import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { RouterModule, Routes } from '@angular/router';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';

const routes: Routes = [
  {
    path: '', component: MainComponent, children: [
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: () => import('../../pages/home/home.module').then(m => m.HomeModule) },
      { path: 'teams/:teamid', loadChildren: () => import('../../pages/teamsdetail/teamsdetail.module').then(m => m.TeamsdetailModule) },
      // { path: 'change-password', loadChildren: () => import('../../pages/main/chpwd/chpwd.module').then(m => m.ChpwdModule) },
    ]
  }
];

@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    BsDropdownModule.forRoot(),
  ]
})
export class MainModule { }
