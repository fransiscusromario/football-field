import { Component, OnInit } from '@angular/core';
import { FootballService } from 'src/app/services/football.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  area: any;
  public teams:Array<any> = ['kosong'];
  coba: any;
  showSideBar = false;
  oneAtATime = true;
  
  constructor(
    public footballService : FootballService
  ) { }

  ngOnInit(): void {
    this.footballService.getAllData().subscribe((response:any) => {
      this.area = response.areas;
    })
  }

  click(id:any){
    this.footballService.getAllTeamsByArea(id).subscribe((response:any) => {
      this.teams = response.teams;
    });
  }

  reset(){
    this.teams = ['kosong'];
  }
}
