import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FootballService } from 'src/app/services/football.service';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';

@Component({
  selector: 'app-teamsdetail',
  templateUrl: './teamsdetail.component.html',
  styleUrls: ['./teamsdetail.component.scss']
})
export class TeamsdetailComponent implements OnInit {
  teamId : any;
  teamInfo : any;
  bsModalRef : BsModalRef;

  constructor(
    private activatedRoute: ActivatedRoute,
    private footballService : FootballService,
    private modalService: BsModalService,
  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params: any) => {
      if (params.get('teamid')) {
        this.teamId = params.get('teamid');
        this.getData();
      } 
    })
  }

  getData(){
    this.footballService.getOneTeam(this.teamId).subscribe((response: any)=> {
      this.teamInfo = response;
    })
  }

  openPlayerDetails(id:number){
    const initialState = {
      id: id
    }
    this.bsModalRef = this.modalService.show(
      PlayerdetailsComponent, { initialState, class: 'modal-lg', backdrop: 'static' }
    );
  }
}
