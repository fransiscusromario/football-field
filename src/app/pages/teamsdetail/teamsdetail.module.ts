import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsdetailComponent } from './teamsdetail.component';
import { RouterModule, Routes } from '@angular/router';
import { PlayerdetailsComponent } from './playerdetails/playerdetails.component';
import { ModalModule } from 'ngx-bootstrap/modal';

const routes: Routes = [
  {
    path: '', component: TeamsdetailComponent
  }
];

@NgModule({
  declarations: [
    TeamsdetailComponent,
    PlayerdetailsComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot()
  ]
})
export class TeamsdetailModule { }
