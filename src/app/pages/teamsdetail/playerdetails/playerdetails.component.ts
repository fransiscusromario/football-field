import { Component, Input, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { FootballService } from 'src/app/services/football.service';

@Component({
  selector: 'app-playerdetails',
  templateUrl: './playerdetails.component.html',
  styleUrls: ['./playerdetails.component.scss']
})
export class PlayerdetailsComponent implements OnInit {
  @Input() id:any; 
  playerDetail:any;
  modalRef?: BsModalRef;

  constructor(
    private footballService : FootballService,
    public bsModalRef: BsModalRef,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.footballService.getPlayerDetail(this.id).subscribe((response:any)=> {
      this.playerDetail = response;
      console.log(this.playerDetail);
    })
  }

  modalHide(){
    this.bsModalRef.hide();
  }
}
