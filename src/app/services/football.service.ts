import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { api_url } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FootballService {

  constructor(private http: HttpClient) { }

  getAllData(){
    return this.http.get(`${api_url}/areas`);
  }

  getAllTeamsByArea(id:any){
    return this.http.get(`${api_url}/teams?areas=${id}`)
  }

  getOneTeam(id:any){
    return this.http.get(`${api_url}/teams/${id}`)
  }

  getPlayerDetail(id:any){
    return this.http.get(`${api_url}/players/${id}`)
  }

}
